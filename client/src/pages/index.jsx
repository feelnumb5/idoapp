import React, { Component } from 'react'
import { Showcase } from '../components/showcase.jsx'
import * as L from "../components/layout";

export class Index extends Component {
    render() {
        return (
            <React.Fragment>

                
                <L.Header  >
                    <L.Navbar title="I Do App" >
                        <div className="navbar-start">
                            <L.NavbarLink className="navbar-item" to="/" > Index</L.NavbarLink>
                            <L.NavbarLink className="navbar-item" to="/home" > home</L.NavbarLink>
                            <L.NavbarLink className="navbar-item" to="/contact" > contact</L.NavbarLink>
                            <L.NavbarLink className="navbar-item" to="/about" > about</L.NavbarLink>

                            <a className="navbar-item"> Documentation </a>

                            <div className="navbar-item has-dropdown is-hoverable">
                                <a className="navbar-link"> More</a>
                                <div className="navbar-dropdown">
                                    <a className="navbar-item">About</a>
                                    <a className="navbar-item">Jobs</a>
                                    <a className="navbar-item">Contact</a>
                                    <hr className="navbar-divider" />
                                    <a className="navbar-item">Report an issue </a>
                                </div>
                            </div>
                        </div>

                        <div className="navbar-end">
                            <div className="navbar-item">
                                <div className="buttons">
                                    <a className="button is-primary">
                                        <strong>Sign up</strong>
                                    </a>
                                    <a className="button is-light">Log in</a>
                                </div>
                            </div>
                        </div>

                    </L.Navbar>
                </L.Header>

                <Showcase  >
                    <strong>50000</strong>
                </Showcase>
                <Showcase  >
                    <strong>50000</strong>
                </Showcase>
            </React.Fragment >
        )
    }
}
