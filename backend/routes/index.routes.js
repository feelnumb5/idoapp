const route = require("express").Router();
const IndexController = require('../controllers/index.controller.js');

module.exports = () => {
    route.get('/api/post', IndexController.getPost);
    route.get('/api/post/:id', IndexController.getPost);
    route.post('/api/post', IndexController.createPost);
    route.put('/api/post/:id', IndexController.createPost);
    route.delete('/api/post/:id', IndexController.deletePost);
    return route
};

