import React, { Component } from 'react'



export class MouseMoveMent extends Component {
    state = {
        x: null,
        y: null
    }

    handleMove = (e) => {
        this.setState({
            x: e.clientX,
            y: e.clientY,
        })
    }

    render() {
        const { x, y } = this.state
        return (
            <div onMouseMove={this.handleMove}  >
                the mouse  is at X:{x} Y:{y}
                {this.props.children}
            </div>
        )
    }
}
