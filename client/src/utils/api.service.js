export class HttpClient {

    constructor(baseURL) {
        this.URL = baseURL;
    }


    get = (url, params, callback) => {
        let baseUrl = this.URL + url;
        let trueUrl = (params) ? this.withQuery(baseUrl, params) : baseUrl;
        return fetch(trueUrl, this.requestOptions("GET"))
            .then(res => {
                return res.json();
            })
            .then(res => {
                if (callback)
                    return callback(res)
            })
            .catch(err => {
                throw err;
            });

    }

    post = (url, data, callback) => {
        return fetch(`${this.URL}${url}`, this.requestOptions("POST", data))
            .then(res => {
                return res.json();
            })
            .then(res => {
                if (callback)
                    return callback(res);
            })
            .catch(err => {
                throw new Error('Something went wrong' + err);
            });

    }

    delete = (url, cond, callback) => {
        return fetch(`${this.URL + url + "/" + cond}`, this.requestOptions("DELETE"))
            .then(res => {
                return res.json();
            })
            .then(res => {
                if (callback)
                    return callback(res);
            })
            .catch(err => {
                console.log("Fetch Error :", err);
            });
    }

    put = (url, data, callback) => {
        return fetch(`${this.URL}${url}`, this.requestOptions("PUT", data))
            .then(res => {
                return res.json();
            })
            .then(res => {
                if (callback)
                    return callback(res);
            })
            .catch(err => {
                throw new Error('Something went wrong' + err);
            });
    }

    requestOptions(method = 'GET', data = null) {
        var options = {
            method: method,
            mode: "cors", // no-cors, cors, *same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: "omit", // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
            },
        }

        if (data) {
            options.body = JSON.stringify(data)
        }
        return options;
    }

    withQuery(url, params) {
        let newurl = new URL(url)
        Object.keys(params).forEach(key => newurl.searchParams.append(key, params[key]))
        return newurl;
    }




}