const pool = require('../config/database.js');

module.exports = {

    get: (req, res, callback) => {
        let query = "SELECT * FROM `data`"; // query database to get all the players
        if (req.params.id) {
            query += `WHERE id = '${req.params.id}'  LIMIT 1`
        } else {
            query += " ORDER BY id ASC";
        }
        // execute query
        pool.query(query, callback);
    },

    create: (req, res, callback) => {
        let post = { text: req.body.text }
        let sql = "INSERT INTO data SET ?"; // query database to get all the players
        // execute query
        pool.query(sql, post, callback);
    },

    update: (req, res, callback) => {
        let post = { text: req.body.text, id: req.params.id }
        let sql = `UPDATE  data SET text =  ${post.text} WHERE id =${post.id}`; // query database to get all the players
        // execute query
        pool.query(sql, callback);
    },

    delete: (req, res, callback) => {
        let post = { id: req.params.id }
        let sql = `DELETE  FROM  data  WHERE id =${post.id}`; // query database to get all the players
        // execute query
        pool.query(sql, callback);
    },
};
