import React, { Component } from 'react'
import { KeypressContianer } from '../components/KeypressContianer.jsx'
import { TextToggle } from '../components/form';


const initialItemState = () => {
    return {
        I: 1,
        II: 2,
        III: 3,
        IV: 4,
        V: 5,
        VI: 6,
        VII: 7,
    }
}

const initialState = () => {
    let tmpArr = [];
    let i = 0;
    while (i < 30) {
        tmpArr = [...tmpArr, initialItemState()]
        i++
    }
    return tmpArr
}

export class Test extends Component {

    constructor(props) {
        super(props)
    }

    state = {
        data: initialState()
    }

    onChange = (e, idx) => {
        const { value, name } = e.target
        console.log(value, name, idx);

        this.setState((prevState) => {
            let data =  prevState.data;
            data[idx][name] = value
            return {
                data:data
            }
        })

    }


    keyDown = (e, idx) => {
        let key = e.key, name = e.target.name
        let count = this.state.data.length;

        switch (key) {
            case 'ArrowUp':
                let prevIdx = (idx);
                if (prevIdx == 0) {
                    prevIdx = count
                }
                let keyName = `${prevIdx}_inp_${name}`;
                let elementUp = this.refs[keyName];

                return elementUp.refs[keyName].focus();
            case 'ArrowDown':
                let next = idx + 2;
                if (next > count) {
                    next = 1
                }

                let keyNameDown = `${next}_inp_${name}`;
                let elementDown = this.refs[keyNameDown];
                return elementDown.refs[keyNameDown].focus();
            case 'ArrowLeft':

                break;

            case 'ArrowRight':

                break;

        }

    }

    
    render() {
        return (
            <KeypressContianer >
                <div className="table-container custome-container-table">
                    <table className="table custom">
                        <thead>
                            <tr>
                                <th>#</th>
                                {
                                    Object.keys(initialItemState()).map(i => {
                                        return <th key={i}>{i}</th>
                                    })
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.data.map((c, idx) => {

                                    let names = Object.keys(c);
                                    let e = names.map((i, nIdx) => {
                                        let name = `${idx + 1}_inp_${i}`;
                                        return <td key={'td_' + name}> <TextToggle
                                            onKeyDown={_ => {
                                                return this.keyDown(_, idx)
                                            }}
                                            className="input is-small"
                                            ref={name}
                                            onChange={_ => this.onChange(_, idx)}
                                            value={c[i]}
                                            name={names[nIdx]}
                                            refName={name}
                                        />
                                        </td>

                                    })

                                    return <tr key={'tr' + idx}>
                                        <td><span>{idx + 1}</span> </td>
                                        {e}
                                    </tr>
                                })
                            }
                        </tbody>
                    </table>
                </div>

            </KeypressContianer>
        )
    }
}
