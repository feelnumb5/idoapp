import React from "react";
import PropTypes from "prop-types";

const toLocal = (value) => {
    let valueAsNumber = parseFloat(value);
    if (valueAsNumber) {
        return valueAsNumber.toLocaleString('en-EU', { minimumFractionDigits: 3 })
    }
    return '';
}

export class TextNumber extends React.Component {

    constructor(props) {
        super(props);
        this.state = { isEditing: false };
    }

    onChange(_) {
        this.props.onChange(_);
    }



    toggleEditing() {
        this.setState({ isEditing: !this.state.isEditing });
    }

    render() {
        return (
            <div className="input-field">
                <label htmlFor={this.props.name}>{this.props.label}</label>
                {this.state.isEditing ? (
                    <input
                        type="number"
                        name={this.props.name}
                        value={this.props.value}
                        onChange={this.onChange.bind(this)}
                        onBlur={this.toggleEditing.bind(this)}
                    />
                ) : (
                        <input
                            type="text"
                            name={this.props.name}
                            value={toLocal(this.props.value)}
                            onFocus={this.toggleEditing.bind(this)}
                            readOnly
                        />
                    )}
            </div>
        );
    }
}

TextNumber.propTypes = {
    name: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    label: PropTypes.string,
};
