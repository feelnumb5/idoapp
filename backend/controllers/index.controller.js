const post = require('../models/post.model.js');

class IndexController {

    static getPost(req, res) {
        post.get(req, res, (err, result) => {
            if (err) {
                res.status(404).send(err);
            } else {
                res.status(200).send(result);
            }
        });
    }

    static createPost(req, res) {

        let fn = (req.body.id) ? post.update : post.create;

        fn(req, res, (err, result) => {
            if (err) {
                res.status(404).send(err);
            } else {

                let message = {
                    text: req.body.text,
                    id: result.insertId
                }
                res.status(200).send(message);
            }
        });
    }

    static deletePost(req, res) {
        post.delete(req, res, (err, result) => {
            if (err) {
                res.status(404).send(err);
            } else {
                let message = {
                    result: "ok",
                    affectedRows: 1
                }
                if (result.affectedRows == 1) {
                    res.status(200).send(message);
                } else {
                    message.affectedRows = 0;
                    res.status(200).send(message);
                }
            }
        });
    }



}

module.exports = IndexController;
