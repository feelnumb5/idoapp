import React, { Component } from 'react'
import { Modal } from '../components/modal';

export class Contact extends Component {
    state = {
        show: false,
        show2: false
    }

    onShowModal = () => {
        this.setState({ show: !this.state.show })
    }


    onShow2Modal = () => {
        this.setState({ show2: !this.state.show2 })
    }

    render() {
        const { show, show2 } = this.state
        return (
            <div className="container">
                <button onClick={this.onShowModal} className="waves-effect waves-light btn modal-trigger">Modal</button>

                <Modal onClose={this.onShowModal} show={show}  >
                    <label>5500</label>
                </Modal>
            </div>
        )
    }
}
