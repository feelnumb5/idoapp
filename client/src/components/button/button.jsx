import React, { Component } from 'react'
import PropTypes from 'prop-types'

const h = tag => (props = {}) => (...childen) => ({
    tag, prps, childen, key: props.key
})

const nodeToDom = ({ tag, props, childen }) => {
    const el = document.createElement(tag);

    for (let key in props) {
        const value = props[key]
        if (typeof value === 'function') {
            el[key] = value
        } else {
            el.setAttribute(key, value)
        }
    }

    childen.forEach(child => {
        if (typeof child === 'string') {
            el.appendChild(document.createTextNode(child))
        } else {
            el.appendChild(nodeToDom(child));
        }
    })

    return;

}


export class Button extends Component {

    state = {
        label: "",
        loading: false
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const { uuid } = this.props

        if (!Button.self[uuid]) {
            Button.self.push(this);
        }
    }

    onClick = e => {
        this.setState({ loading: true })
        setTimeout(() => {
            this.setState({ loading: false }, () => {
                const { onClick } = this.props
                if (onClick) onClick(e)
            })
        }, 500);
    }


    render() {
        const { color, text } = this.props
        const { label, loading } = this.state
        const disabled = (loading) ? "disabled" : "";
        return (
            <React.Fragment>
                <button onClick={this.onClick} className={`waves-effect waves-light btn ${color} ${disabled}`} >
                    {text}
                </button>
                <label >{label}</label>
            </React.Fragment>

        )
    }
}

Button.propTypes = {
    color: PropTypes.string
}


Button.defaultProps = {
    self: [],
    color: "blue darken-1"
}

Button.self = [];


