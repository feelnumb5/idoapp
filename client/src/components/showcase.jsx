import React, { Component } from 'react'

export class Showcase extends Component {

    render() {
        return (
            <section className="section">
                {this.props.children}
            </section>
        )
    }
}
