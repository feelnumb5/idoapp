let registeredShortcuts = [];

export class Shortcut extends React.Component {
    componentDidMount() {
        if (!registeredShortcuts.includes(this.props.name)) {
            registeredShortcuts = [
                ...registeredShortcuts,
                this.props.name,
            ];
        }
    }
    componentWillUnmount() {
        registeredShortcuts = [
            ...registeredShortcuts.filter(x => x !== this.props.name),
        ];
    }
    render() {
        return this.props.children || null;
    }
}
