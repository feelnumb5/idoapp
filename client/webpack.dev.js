const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');
const path = require('path');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        hot: true,
        contentBase: path.resolve(__dirname, 'dist'),
        historyApiFallback: true // Allow refreshing of the page
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(), // Enable hot reloading
    ]
});