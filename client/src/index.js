import { AppContainer } from 'react-hot-loader';
import { BrowserRouter, Route, Switch, Link, NavLink, } from 'react-router-dom';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './app.jsx';
import { About, Contact, Home, Login, Index,Test } from "./page.js"

import "../public/css/default.css"

const mountPoint = document.getElementById("root");
const render = (AppComponent) => {
    ReactDOM.render(
        <AppContainer>
            <BrowserRouter>
                <AppComponent >
                    <Switch>
                        <Route path='/' exact component={Index} />
                        <Route path='/test' component={Test} />
                        <Route path='/home' component={Home} />
                        <Route path='/about' component={About} />
                        <Route path='/contact' component={Contact} />
                        <Route path='/login' component={Login} />
                        <Route component={() => { return <h1>Not Found </h1> }} />
                    </Switch>
                </AppComponent >
            </BrowserRouter>
        </AppContainer >,
        mountPoint,
    );
};

render(App);

if (module.hot) {
    module.hot.accept('./app.jsx', () => render(App));
}
