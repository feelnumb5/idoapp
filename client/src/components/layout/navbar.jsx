import React, { Component } from "react";
import { Link } from 'react-router-dom';

export class Navbar extends Component {
    state = {
        isActive: false
    }

    constructor(props) {
        super(props)

        this.hamburgur = React.createRef();
    }

    componentDidMount() {

    }

    onClickHamburgur = (e) => {

        this.setState((state) => {
            return { isActive: !state.isActive }
        })
    }

    render() {
        const { isActive } = this.state


        let clsName = (isActive) ? 'is-active' : '';

        return (
            <nav className={`navbar`} role="navigation" aria-label="main navigation">

                <div className="navbar-brand">
                    <a className="navbar-item" href="#!#">
                        {/* <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28" /> */}
                        <label style={{ color: 'hsl(0, 0%, 21%)' }}> {this.props.title}</label>
                    </a>

                    <a className={clsName} onClick={this.onClickHamburgur} role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navMenu">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>

                <div className={"navbar-menu" + clsName}>

                    {this.props.children}

                </div>
            </nav>
        )
    }
}



export const NavbarLink = ({ children, ...props }) => {
    return <Link   {...props} > {children}</Link>
}