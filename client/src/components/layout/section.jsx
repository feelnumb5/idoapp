import React, { Component } from 'react'

const sectionSlyte = {
    width: '90%',
    margin: 'auto',
}

export class Section extends Component {
    render() {
        return (
            <section style={sectionSlyte} >
                {this.props.children}
            </section>
        )
    }
}


