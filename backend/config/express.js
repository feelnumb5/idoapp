require("dotenv").config();

const express = require("express");
const path = require("path");
const logger = require("morgan");
const cors = require("cors");

const indexRoute = require('../routes/index.routes');


module.exports = () => {

  const app = express();

  app.use(express.static(path.join(__dirname, "../public")));

  app.use(cors());
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(logger("dev"));

  app.use(indexRoute());

  // app.use("/static", express.static("public"));
  return app;
};
