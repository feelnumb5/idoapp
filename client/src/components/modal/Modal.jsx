import React, { Component } from 'react'
import ReactDOM from 'react-dom';
const modalRoot = document.getElementById("modal-root");

const backdropStyle = {
    position: 'fixed',
    left: '0',
    bottom: '0',
    right: ' 0',
    height: '125%',
    width: '100%',
    zIndex: '999',
    backgroundColor: 'rgba(0,0,0,0.5)',
}

const modalStyle = {
    position: 'fixed',
    left: '0',
    right: '0',
    backgroundColor: 'white',
    padding: '0',
    maxHeight: '70%',
    zIndex: '1000',
    width: '80%',
    margin: 'auto',
    minHeight: '70vh',
    overflowY: 'auto',
    borderRadius: '2px',
    border: 'solid 1px transparent',
    top: '5%',
}

const modalStyleTitle = {
    height: "32px",
    position: "absolute",
    top: "0",
    left: "0",
    right: "0",
    cursor: "default",
    userSelect: "none",
    padding: '0.55vh'
}

const modalStyleIconTitle = {
    right: "100px",
    position: 'inherit',
    color: 'white',
    margin: 'auto',
}

const initialState = () => {
    return {
        modalStyle,
        backdropStyle
    }
}

export class Modal extends Component {

    state = initialState();

    constructor(props) {
        super(props)
        this.el = document.createElement("div");
    }

    componentDidMount() {
        document.addEventListener('keyup', this.keyUp, false)
        modalRoot.appendChild(this.el);
    }

    componentWillUnmount() {
        document.addEventListener('keyup', this.keyUp, false)
        modalRoot.removeChild(this.el);
    }

    keyUp = (e) => {
        if (e.key == "Escape") {
            this.onClose();
        }
    }

    onClose = () => {
        const { onClose } = this.props
        onClose();
    }

    onClickModal = (e) => {
        e.stopPropagation();
    }

    onExitFullScreen = (e) => {
        this.setState({ modalStyle: { ...modalStyle } })
    }

    onFullScreen = (e) => {
        let mStlye = { ...this.state.modalStyle }
        mStlye.width = '90%';
        mStlye.top = '5%';
        mStlye.minHeight = '85vh';
        this.setState(() => {
            return { modalStyle: mStlye }
        })
    }


    render() {
        const { show } = this.props

        if (!show) {
            return null;
        }

        let minimizeIcon = { ...modalStyleIconTitle }
        minimizeIcon.right = '50px'

        let fullScreenIcon = { ...modalStyleIconTitle }
        fullScreenIcon.right = '27px'

        let closeIcon = { ...modalStyleIconTitle }
        closeIcon.right = '5px'

        let titleStlye = { ...modalStyleIconTitle }
        delete titleStlye.right;
        titleStlye.left = '1vw'



        let modalElement = <div onClick={this.onClose} style={this.state.backdropStyle}>
            <div onClick={this.onClickModal} style={this.state.modalStyle} className="app-modal-page">
                <div style={modalStyleTitle} className="app-modal-title">
                    <span className="app-modal-title-text" style={titleStlye}>{this.props.title || 'Modal'}</span>
                    <span className="app-modal-icon-close" onClick={this.onClose} style={closeIcon}><i className="material-icons">close</i></span>
                    <span className="app-modal-icon-fullscreen-exit" onClick={this.onExitFullScreen} style={minimizeIcon}><i className="material-icons">fullscreen_exit</i></span>
                    <span className="app-modal-icon-fullscreen" onClick={this.onFullScreen} style={fullScreenIcon}><i className="material-icons">fullscreen</i></span>
                </div>
                <br />
                <br />
                <div className="app-modal-content">
                    {this.props.children}
                </div>
                <div className="app-modal-footer">
                    <button onClick={this.onClose} className="waves-effect waves-green btn-flat">Close</button>
                </div>
            </div>
        </div>

        return ReactDOM.createPortal(modalElement, this.el)
    }
}


