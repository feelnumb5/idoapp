import React, { Component } from "react";
// import L from 'react-loadable'

// const Loading = () => (
//   <div>Loading...</div>
// )

// const Loadable = opts => L({
//   loading: Loading,
//   ...opts
// })

// const AsyncHome = Loadable({
//   // เมื่อเข้าสู่ / เราจะ dynamic import โมดูล Home เข้ามาใช้งาน
//   // หาก path ปัจจุบันไม่ใช้ / โมดูล Home จะไม่ถูกโหลดมาทำงาน
//   loader: () => import(/* webpackChunkName: "home" */ './pages/home.jsx'),
// })

// const AsyncAbout = Loadable({
//   loader: () => import(/* webpackChunkName: "about" */ './pages/about.jsx')
// })

// const AsyncContact = Loadable({
//   loader: () => import(/* webpackChunkName: "contact" */ './pages/contact.jsx')
// })

// const AsyncLogin = Loadable({
//   loader: () => import(/* webpackChunkName: "Login" */ './pages/login.jsx')
// })




export default class App extends Component {
  render() {
    return <div className="container-fluid">
      {this.props.children}
    </div>
  }
}






// const bootsystem = () => {
//   const mountPoint = document.getElementById("root");
//   render(<AppContainer><App /></AppContainer>, mountPoint);
// };

// window.addEventListener('DOMContentLoaded', (event) => {

//   // bootsystem();

// });