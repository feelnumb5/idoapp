import React, { Component } from 'react'

export class Header extends Component {

    static defaultProps = {
        title: ""
    }

    render() {

        const { title } = this.props

        return (
            <header >
                {this.props.children}
            </header>
        )
    }
}




