import React, { Component } from 'react'

export class TextToggle extends Component {
    constructor(props) {
        super(props);
        this.state = { isEditing: false };
    }

    shouldComponentUpdate(nextProps, nextState) {
        const { value } = this.props
        const { isEditing } = this.state
        if (value !== nextProps.value || nextState.isEditing !== isEditing) {
            return true
        }
        return false
    }

    onChange = (e) => {
        if (this.props.onChange) {
            this.props.onChange(e);
        }
    }


    toggleEditing = () => {
        this.setState({ isEditing: !this.state.isEditing });
    }

    onKeyPress = (e) => {
        if (e.key == 'Enter') {
            this.toggleEditing();
        }

    }

    render() {
        const { className } = this.props
        let cls = className + ' is-primary'
        console.log(" Render ", this.props.name);
        return (
            <div className="input-field">
                {this.state.isEditing ? (
                    <input
                        type="text"
                        name={this.props.name}
                        ref={this.props.refName}
                        value={this.props.value}
                        onChange={this.onChange}
                        className={className}
                        // onBlur={this.toggleEditing}
                        onKeyPress={this.onKeyPress}
                        onKeyDown={this.props.onKeyDown}
                    />
                ) : (
                        <input
                            type="text"
                            name={this.props.name}
                            ref={this.props.refName}
                            value={this.props.value}
                            // onFocus={this.toggleEditing}
                            onKeyPress={this.onKeyPress}
                            onKeyDown={this.props.onKeyDown}
                            readOnly={true}
                            className={cls}

                        />
                    )}
            </div>
        )
    }
}

