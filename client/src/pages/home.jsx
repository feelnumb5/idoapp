import React, { Component } from 'react'
import { findDOMNode } from "react-dom";

import { HttpClient } from '../utils/index';

const initialState = () => {
    return {
        data: [],
        text: "",
        selectedFile: null,
        fileContent: ""
    }
}

export class Home extends Component {

    api = new HttpClient("http://localhost:5000/")

    state = initialState()

    constructor(props) {
        super(props);
        this.textArea = React.createRef();
        this.img = React.createRef();
    }

    componentDidMount() {
        this.api.get('api/post', { id: 225 }, (res) => {
            this.setState({ data: res });
        }).catch(err => {
            console.log(err);
        })
        // let img = localStorage.getItem('img');
        // if (img) {
        //     let imgComponent = findDOMNode(this.img.current);
        //     imgComponent.setAttribute("src", img)
        //     M.Materialbox.init(imgComponent);

        // }
    }

    onClick = () => {

        const { text } = this.state
        this.api.post('api/post', { text: text }, (res) => {
            this.setState((state, props) => {
                return {
                    data: [...state.data, res],
                    text: ''
                }
            })
        })

    }

    onRemove = (_, id) => {
        _.stopPropagation();
        this.api.delete('api/post', id, (res) => {
            this.setState((state, props) => {
                return { data: state.data.filter(i => i.id != id) }
            })
        }).catch(err => {
            console.log(err);
        })
    }

    onChange = e => {

        this.setState({ [e.target.name]: e.target.value });
    }

    onKeyPress = e => {
        let key = e.key
        if (key === "Enter") {
            this.onClick();
        }
    }

    onOpen = (_, id) => {
        _.stopPropagation();
        this.api.get('api/post/' + id, null, (res) => {
            console.log(res);

        }).catch(err => {
            console.log(err);
        })
    }

    onClickHandler = _ => {
        const data = new FormData();
        data.append('file', this.state.selectedFile)
        fetch('api/post', { // Your POST endpoint
            method: 'POST',
            'Content-Type': 'application/json',
            body: data // This is your file object
        }).then(res => {
            console.log(res);

        })
    }

    onChangeHandler = _ => {

        let file = _.target.files[0] || "";

        if (file == "") {
            let imgComponent = findDOMNode(this.img.current);
            imgComponent.setAttribute("src", "")
            localStorage.setItem('img', "")

            this.setState({ fileContent: "" }, () => {
                M.textareaAutoResize(this.textArea.current)
            })
            return;
        }
        const reader = new FileReader()

        this.setState({ selectedFile: file, loaded: 0, }, () => {
            let type = this.state.selectedFile.type
            let file = this.state.selectedFile
            return new Promise((resolve, reject) => {
                reader.onload = _ => {
                    if (type === "image/png" || type === "image/jpeg") {
                        let imgComponent = findDOMNode(this.img.current);
                        imgComponent.setAttribute("src", _.target.result)
                        localStorage.setItem('img', _.target.result)
                        M.Materialbox.init(imgComponent);
                    } else {
                        this.setState({ fileContent: _.target.result }, () => {
                            M.textareaAutoResize(this.textArea.current)
                        })
                    }

                }

                reader.onerror = err => console.log(err)

                if (file) {
                    if (type === "image/png" || type === "image/jpeg" || type === "application/pdf") {
                        return reader.readAsDataURL(file)
                    }
                    return reader.readAsText(file)
                }


            })

        })
    }


    render() {
        const { data, text, fileContent } = this.state;

        return (
            <div>

                {/* <button onClick={this.onClickHandler} className="btn">Upload</button>
                <div className="file-field input-field">
                    <div className="btn">
                        <span>File</span>
                        <input onChange={this.onChangeHandler} type="file" />
                    </div>


                    <div className="file-path-wrapper">
                        <input className="file-path validate" type="text" placeholder="Upload one or more files" />
                    </div>
                </div>

                <textarea ref={this.textArea} name="fileContent" onChange={this.onChange} value={fileContent} className="materialize-textarea" id="content-target"></textarea>

                <img alt="img-upload" ref={this.img} className="materialboxed responsive-img" src={""}></img>
                */}
                <div className="input-field">
                    <input id="text" onKeyPress={this.onKeyPress} name="text" value={text} onChange={this.onChange} type="text" />
                    <label htmlFor="text">Text</label>
                </div>

                <button className="btn waves-effect waves-light" onClick={this.onClick}>Add</button>

                {Array.from(data).length > 0 && <ul className="collection">

                    {data.map((item, idx) => {
                        let text = (item.text) || " ";

                        return (
                            <li style={{ minHeight: "39px" }} onClick={(_) => { this.onOpen(_, item.id) }} className="collection-item" key={idx}>
                                {text}
                                <a style={{ color: "red" }} onClick={(_) => { this.onRemove(_, item.id) }} href="#!" className="secondary-content "><i className="material-icons">delete</i></a>
                            </li>
                        );
                    })}

                </ul>
                }
            </div>
        )
    }
}
