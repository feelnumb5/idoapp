import React, { Component } from 'react'


const stlyeContainer = {
    width: "80%",
    margin: 'auto',
    overFLow: 'hidden'
}

export const Container = ({ children }) => {
    
    return (<div style={stlyeContainer}>
        {children}
    </div>)

}
