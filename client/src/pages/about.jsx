import React, { Component } from 'react'
import { TextInput, TextNumber } from '../components/form';
import { Button } from '../components/button/button.jsx';
import { MouseMoveMent } from '../components/MouseMoveMent.jsx';
import { Container } from '../components/layout/container.jsx'

const initialSate = () => {
    return {
        value: '',
        valueAsNumber: ''
    }
}

export class About extends Component {

    state = initialSate();

    onChange = (_) => {
        const { value, name, type } = _.target
        if (type === "number") {
            this.setState({ valueAsNumber: value })

        } else {
            this.setState({ value })

        }

    }

    onClick = (_, next) => {

    }

    render() {
        const { value, valueAsNumber } = this.state
        return (
            <MouseMoveMent>
                <Container>
                    <TextInput value={value || ""} onChange={this.onChange} />
                </Container>
                <Container>
                    <TextNumber label={"จำนวนเงิน"} name="other_1" onChange={this.onChange} value={valueAsNumber} />
                </Container>

                <div className="row">
                    <div className="col s4">
                        <Button uuid={"danger"} text={"Danger"} color={"red"} />
                    </div>
                    <div className="col s4">
                        <Button uuid={"info"} text={"Info"} />
                    </div>

                    <div className="col s4">
                        <Button

                            color={'teal accent-3'}

                            onClick={_ => this.onClick(_, () => {
                                console.log("gvk")
                            })}

                            uuid={"Process"}

                            text={"Process"}
                        />

                    </div>

                </div>
            </MouseMoveMent>
        )
    }
}
