import React, { Component } from 'react'

export class TextInput extends Component {

    onChange = (e) => {
        if (this.props.onChange) {
            this.props.onChange(e);
        }
    }

    render() {
        return (
            <div className="input-field">
                <input  {...this.props} onChange={this.onChange} />
                <label ></label>
            </div>
        )
    }
}

