export const BREAKPOINTS = {
    extrasmall: '481px',
    small: '769px',
    medium: '1025px',
    larg: '1350px'
}


export const COLORS = {
    black: '#333',
    gray: '#9baab0',
    lightGray: '#eff4f5',
    teal: '#33a1cc',
    white: '#fff'
}
