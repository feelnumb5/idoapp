const mysql = require('mysql');
const { promisify } = require('util');
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: "root",
    password: "",
    database: "pos"
});

pool.getConnection((err, connection) => {
    console.log("sadasd");
    
    if (err) {
        if (err.code === 'PROTOCAL_CONNNECTION_LOST') {
            console.log('DATABSE CONNECTION WAS CLOSED');
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.log('DATABSE HAS TO MANY CONNETIONS');
        }
        if (err.code === 'ECONNREFUSED') {
            console.log('DATABASE CONNECTION WAS REFUSED');
        }
    }

    if (connection) {
        connection.release();
        console.log('DB CONNECTED');
    }
    return;
})

// promis pool query
pool.query =  promisify(pool.query)
module.exports = pool;


